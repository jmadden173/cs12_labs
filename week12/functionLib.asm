; Data
section     .data
extern  printMSG
extern  printEndl
extern  printReg
extern  printRAX
extern  exitNormal
extern  getRand

; Code 
section     .text


;-----------------------------------------
; declare functions
global mulby4
global printXtimes  
global getRandNum
global getMax


;-----------------------------------------
; the first 6 parameters are in this order
;-----------------------------------------
;param  64bit   32bit   16bit   8bit
;1      rdi     edi     di      dil
;2      rsi     esi     si      sil
;3      rdx     edx     dx      dl
;4      rcx     ecx     cx      cl
;5      r8      r8d     r8w     r8b
;6      r9      r9d     r8w     r8b
;-----------------------------------------

;-----------------------------------------
; return valuesare in
;       rax     eax     ax      al
;-----------------------------------------


;-----------------------------------------
; the callee must preserve these registers 
;-----------------------------------------
;   rbx rbp r12 r13 r14 r15 
; 
;   The stack must be cleared

;-----------------------------------------------------------------------------------
; mulby4
; description:      multiplies the first parameter by 4
;
; precondition:     rdi has the value to be multiplied by 4
;
; postcondition:    rax has the value of rdi shifted left twice
;                   effectively multiplying by 4       
;
; return            rax has the value of rdi shifted left twice
;-----------------------------------------------------------------------------------
mulby4: 
	mov	rax,	rdi
	shl	rax,	2
	ret


;-----------------------------------------------------------------------------------
; printXtimes 
; description:      prints the value in rdi 
;                   the value of rsi times
;
; precondition:     rdi has the value to be displayed
;                   rsi has the number of times to repeat
;
; postcondition:    the value in rdi has been displayed rsi times
;
; return            n/a
;-----------------------------------------------------------------------------------
printXtimes:
	push	rcx

	mov	rcx,	rsi
	printXtimesLoop:
	call	printReg
	loop	printXtimesLoop

	pop	rcx

	ret


;-----------------------------------------------------------------------------------
; getRandNum 
; description:      returns a random number 
;                   between 0 and rdi
;
; precondition:     rdi has the max value to be generated
;
; postcondition:    rax holds a number between 0 and rdi
;           
;
; return            a number between 0 and rdi, in rax
;-----------------------------------------------------------------------------------
getRandNum:
	rdrand	rax
	xor	rdx,	rdx
	div	rdi
	mov	rax,	rdx
	ret


;-----------------------------------------------------------------------------------
; getMax 
; description:      returns a the largest number passed into the function 
;
; precondition:     rdi, rsi, rdx, rcx, r8 and r9 have numbers in them
;
; postcondition:    rax has the largest value passed in above
;           
;
; return            rax has the largest value passed in above
;-----------------------------------------------------------------------------------
getMax:
	xor	rax,	rax

	mov	rax,	rdi

	cmp	rsi,	rax
	jg	rsiG
	rsiE:

	cmp	rdx,	rax
	jg	rdxG
	rdxE:

	cmp	rcx,	rax
	jg	rcxG
	rcxE:

	cmp	r8,	rax
	jg	r8G
	r8E:

	cmp	r9,	rax
	jg	r9G
	r9E:


	jmp end

	rsiG:	
	mov	rax,	rsi
	jmp	rsiE

	rdxG:
	mov	rax,	rdx
	jmp	rdxE

	rcxG:
	mov	rax,	rcx
	jmp	rcxE

	r8G:
	mov	rax,	r8
	jmp	r8E

	r9G:
	mov	rax,	r9
	jmp	r9E


	end:
	ret
    

