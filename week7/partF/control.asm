; Data
section 	.data
extern printMSG
extern printByteArray
extern getQuad
extern printRAX
extern printRBX
extern printRCX
extern printRDX
extern exitNormal

; Code 
section		.text

prompt		db	"Enter up to a quadword in hex:"
numprompt	dq	32

countchar	db	"Count"
countcharcount	dq	0x5
	
global _start

_start:
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG


	; Prompt the user for input	
	mov	rsi,	prompt
	mov	rdx,	[numprompt]
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG

	
	; Get user input
	call	getQuad
	call	printRAX


	; rbx is the domain
	mov	rbx,	0x8000000000000000
	; rcx is the guess
	mov	rcx,	rbx
	; rdx is the counter
	mov	rdx,	0x0


	bs:
	; Counter
	inc	rdx

	; Print out the guess
	call	printRCX

	; Shift
	shr	rbx,	1

	cmp	rax,	rcx

	; Jump Above
	ja	above

	; Jump Below
	jb	below

	jne	bs
	jmp	bsend


	above:
	add	rcx,	rbx
	jmp	bs


	below:
	sub	rcx,	rbx	
	jmp bs


	bsend:
	mov	rdi,	0x0
	call	printMSG

	; Display the final guess
	call	printRAX

	mov	rax,	rdx

	mov	rsi,	countchar
	mov	rdx,	[countcharcount]
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG

	; Display the count
	call	printRAX
	
	call	exitNormal
