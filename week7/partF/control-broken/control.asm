; Data
section 	.data
extern printMSG
extern getQuad
extern printByteArray
extern printEndl
extern printRAX
extern printRBX
extern exitNormal

prompt		db	"Enter up to a quadword in hex:"
numprompt	dq	32

; Code 
section		.text
	
global _start

_start:
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rbx,	0x8000000000000000

	mov	rcx,	0x0
	
	mov	rsi,	prompt
	mov	rdx,	[numprompt]
	call	printByteArray
	call	printEndl

	call	getQuad
	call	printRAX

	; Start of a loop
	loopstart:
	
	; Reduce by half
	shr	rbx,	1

	cmp	rax,	rbx
	jg	reduceguess
	jl	increaseguess
	je	endloop

	jmp	loopstart


	reduceguess:
	dec	rbx
	jmp	loopstart


	increaseguess:
	inc	rbx
	jmp	loopstart


	endloop:
	call	printRBX


	call	exitNormal
