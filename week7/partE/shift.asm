; Data
section 	.data
extern printMSG
extern printRAX
extern exitNormal

; Code 
section		.text
	
global _start

_start:
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	; shl Shift
	mov rdi, 0x12
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov rdi, 0x14
	call printMSG
	
	mov rdi, 0x0
	call printMSG


	mov	rax,	0x1
	shl	rax,	1

	call	printRAX	
	
	shl	rax,	1

	call	printRAX	
	
	shl	rax,	1

	call	printRAX


	; shr Shift	
	mov rdi, 0x15
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rax,	0x1000
	shr	rax,	1

	call	printRAX	
	
	shr	rax,	1

	call	printRAX	
	
	shr	rax,	1

	call	printRAX


	; sal Shift	
	mov rdi, 0x14
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rax,	0x1
	sal	rax,	1

	call	printRAX	
	
	sal	rax,	1

	call	printRAX	
	
	sal	rax,	1

	call	printRAX


	; sar Shift	
	mov rdi, 0x15
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rax,	0x8000000000000008
	sar	rax,	1

	call	printRAX	
	
	sar	rax,	1

	call	printRAX	
	
	sar	rax,	1

	call	printRAX


	; rol Shift	
	mov rdi, 0x14
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rax,	0xF000000000000000
	rol	rax,	1

	call	printRAX	
	
	rol	rax,	1

	call	printRAX	
	
	rol	rax,	1

	call	printRAX


	; ror Shift	
	mov rdi, 0x15
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	mov	rax,	0xF
	ror	rax,	1

	call	printRAX	
	
	ror	rax,	1

	call	printRAX	
	
	ror	rax,	1

	call	printRAX
	call	exitNormal
