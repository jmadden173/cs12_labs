; Data
section 	.data

; external function references
extern exitNormal

extern printABCD
extern printMSG
extern printRAX
extern printRBX
extern printRCX
extern printRDX
extern printReg

aVal	dq	0xa0a0a0a
bVal 	dq	0x0b0b0b0
cVal 	dq	0xCCCC
dVal	dq	0x12345678DDDDDDDD

; Code 
section		.text
	
global _start

_start:
	; Print CS12 with new line
	mov	rdi,	0xD
	call	printMSG

	mov	rdi,	0
	call	printMSG


	; Print MOV with new line
	mov	rdi,	1
	call	printMSG
	
	mov	rdi,	0
	call	printMSG


	; Print RAX and the value	
	mov	rdi,	9
	call	printMSG

	mov	rdi,	8
	call	printMSG	

	mov 	rax, 	[aVal]
	call 	printRAX

	; Print RBX and the value
	mov	rdi,	10
	call	printMSG

	mov	rdi,	8
	call	printMSG

	mov	rbx,	[bVal]
	call	printRBX

	
	; Print RCX and the value
	mov	rdi,	11
	call	printMSG

	mov	rdi,	8
	call	printMSG

	mov	rcx,	[cVal]
	call	printRCX


	; Print RDX and the value
	mov	rdi,	12
	call	printMSG

	mov	rdi,	8
	call	printMSG

	mov	rdx,	[dVal]
	call	printRDX	


	; Print RAX,RBX,RCX,RDX all at once
	call	printABCD


	call	exitNormal
