; Data
section 	.data
extern printMSG
extern printRAX
extern printRDX
extern exitNormal

; Code 
section		.text

global _start

_start:
	; Print CS12
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	; Print ADD
	mov	rdi,	0x2
	call	printMSG
	
	mov	rdi,	0x0
	call	printMSG
	

	; Basic ADD statement
	mov	rax,	0x0a
	call	printRAX

	add	rax,	0x0a
	call	printRAX

	add	rax,	0x0a
	call	printRAX


	; Add with carry
	mov	al,	0xFF
	add	al,	0x1
	adc	al,	0x1
	call	printRAX

	; Increment RAX
	inc	rax
	call	printRAX

	; Subtract
	mov	rdi,	0x3
	call	printMSG

	mov	rdi,	0x0
	call	printMSG

	sub	rax,	0xa
	call	printRAX

	
	; Decrement
	dec	rax
	call	printRAX


	; Multiplication
	mov	rdi,	0x4
	call	printMSG

	mov	rdi,	0x0
	call	printMSG

	mov	rax,	0x80
	mov	rbx,	0xa
	mul	rbx

	call	printRDX
	call	printRAX

	mov	rax,	0x8000000001
	mov	rbx,	0x8000000000

	mul	rbx

	call	printRDX
	call	printRAX

	; Signed Multiplication
	xor	rax,	rax
	mov	al,	0x8f
	mov	bl,	0x2
	mul	bl

	call	printRAX

	xor	rax,	rax
	mov	al,	0x8f
	mov	bl,	0x2
	imul	bl

	call	printRAX

	
	; Division
	mov	rdi,	0x5
	call	printMSG

	mov	rdi,	0x0
	call	printMSG

	mov	rax,	0xFFFA
	mov	rdx,	0x0
	mov	rbx,	0xF

	div	rbx
	
	call	printRAX
	call	printRDX

	
	; Signed Division
	mov	rdi,	0x5
	call	printMSG

	mov	rdi,	0x0
	call	printMSG

	mov	ax,	-22
	mov	bh,	0x5

	idiv	bh

	call	printRAX
	
	call	exitNormal
