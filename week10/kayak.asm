; BSS
section		.bss
input	resb	25
reverse	resb	25


; Data
section 	.data
extern printMSG
extern exitNormal
extern printByteArray
extern getByteArray


prompt		db	"Enter a word and I will tell you if it is a palindrome"
promptLen	dq	54

promptNotPal	db	"The word is not a palendrome"
promptNotPalLen	dq	28

promptPal	db	"The word is a palendrome"
promptPalLen	dq	24

; Code 
section		.text
	
global _start

_start:
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG

	; Prompt the user
	mov	rsi,	prompt
	mov	rdx,	[promptLen]
	call	printByteArray

	mov	rdi,	0x0	; Endl
	call	printMSG


	; Get the user input
	mov	rsi,	input
	mov	rdx,	25
	call	getByteArray


	; Get the length of the string
	mov	rcx,	0x25
	getLen:
	mov	rax,	[input+rcx]
	dec	rcx	
	cmp	rax,	0x0a
	jne	getLen

	inc	rcx
	mov	rbx,	rcx
	
	; Pushes everything to the stack
	mov	rcx,	rbx
	reversePush:
	mov	al,	[input-1+rcx]
	push	rax
	loop	reversePush

	; Pops everything to reverse resb
	; should be reversed afterwords
	mov	rcx,	rbx
	reversePop:
	pop	rax
	mov	[reverse-1+rcx],	al
	loop	reversePop



	mov	rcx,	rbx
	checkPal:	
	mov	al,	[input-1+rcx]
	cmp	al,	[reverse-1+rcx]
	jne	notPal
	dec	rcx
	cmp	rcx,	0x0
	jne	checkPal
	je	pal




	pal:
	mov	rsi,	promptPal
	mov	rdx,	[promptPalLen]
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG

	jmp	end




	notPal:
	mov	rsi,	promptNotPal
	mov	rdx,	[promptNotPalLen]
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG

	jmp	end


	end:


		
	
	call	exitNormal
