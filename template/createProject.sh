#!/bin/bash

if [ $# -eq 1 ] && [ ! -d "../$1" ]
then
    echo Creating ../$1
    mkdir ../$1

    echo Creating $1.asm
    cp ./template.asm ../$1/$1.asm

    echo Moving libaries
    cp -r ./cs12Lib ../$1/

    echo Copying Makefile
    sed "s/template/$1/g" Makefile > ../$1/Makefile

else
    echo
    echo usage:
    echo ./createProject projectName
    echo "    where projectName is the name of the directory to create"
    echo "    and the projectName directory does not exist"
    echo
fi
