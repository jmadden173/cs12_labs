; BSS
section .bss
input	resb	5
sort	resb	5


; Data
section 	.data
extern printMSG
extern getByteArray
extern printByteArray
extern exitNormal

promptMsg	db	"Input five characters"
promptMsgLen	dq	21


reverseMsg	db	"Reverse"
reverseMsgLen	dq	7


sortedMsg	db	"Sorted"
sortedMsgLen	dq	6


; Code 
section		.text

global _start
_start:
	mov rdi, 0xd
	call printMSG
	
	mov rdi, 0x0
	call printMSG



	;--------------------------------------
	; Asks the user for input
	; of a five character byte
	; array
	mov	rsi,	promptMsg
	mov	rdx,	[promptMsgLen]
	call	printByteArray

	mov	rdi, 0x0
	call	printMSG



	;--------------------------------------
	; Gets the user input for
	; a five character byte
	; array
	mov	rsi,	input
	mov	rdx,	0x5
	call	getByteArray



	;--------------------------------------
	; Prints Reverse
	mov	rsi,	reverseMsg
	mov	rdx, 	[reverseMsgLen]
	call	printByteArray
	
	mov	rsi,	0x0
	call	printMSG



	;--------------------------------------
	; Reverses the contents of input

	; Sets rdx for a counter
	mov	rcx,	0x4
	mov	rdx,	0x0

	reverse:
	; Move items from input variables
	mov	al,	byte [input+rdx]
	mov	bl,	byte [input+rcx]

	; Set elements of the input array
	mov	byte [input+rdx],	bl
	mov	byte [input+rcx],	al

	dec	rcx
	inc	rdx

	cmp	rcx,	rdx
	jg	reverse


	; Sets all value to 0
	xor	rax,	rax
	xor	rbx,	rbx
	xor	rcx,	rcx
	xor	rdx,	rdx

	mov	rsi,	input
	mov	rdx,	5
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG



	;--------------------------------------
	; Prints Sorted
	mov	rsi,	sortedMsg
	mov	rdx,	[sortedMsgLen]
	call	printByteArray

	mov	rsi,	0x0
	call	printMSG



	;--------------------------------------
	; Sorts the contents of input

	; Reset the counter
	mov	rcx,	0x0		; Outer Loop

	; ----------
	; OUTER LOOP
	; ----------
	; Start the sorted loop
	; Loops through all the characters to move
	sortedOuter:


	; Inc outer loop counter
	inc	rcx

	; Reset the inner loop counter
	mov	rdx,	0x0	; Inner Loop


	; Set to a value less than all possible inputs to compare to
	mov	rbx,	0x0


	; ----------
	; INNER LOOP
	; ----------
	; Loops through all characters to check
	sortedInner:

	; Inc my innner loop counter
	inc rdx

	; Compare the inner counter to the outer counter value
	cmp	bl,	[input-1+rdx]
	; If the second value is greater move into sort
	jb	sortedAbove
	postCMP:



	; Check if the inner loop is done
	cmp	rdx,	0x5
	; If done then continue
	; Else got to sorted inner
	jne	sortedInner



	jmp	sortedMove
	postMove:
	
	; Check if it is at the end of the outer loop
	cmp	rcx,	0x5
	je	sortedEnd
	; After each of the outer loop move the lowest
	; value into the place in sort resb
	jne	sortedOuter



	; If it is above then move that value into
	; bl for the next comparison
	sortedAbove:
	mov	bl,	[input-1+rdx]
	mov	rax,	rdx
	jmp	postCMP



	sortedMove:
	mov	[sort-1+rcx],	bl
	xor	rdx,	rdx
	mov	[input-1+rax],	dl
	jmp	postMove



	; Gets jump to once the loops are done
	sortedEnd:
	mov	rsi,	sort
	mov	rdx,	5
	call	printByteArray

	mov	rdi,	0x0
	call	printMSG


	
	call	exitNormal
